name := "milk-calculator"

version := "0.2"

scalaVersion := "2.13.0"

libraryDependencies += "joda-time" % "joda-time" % "2.10.2"  withSources() withJavadoc()
lazy val commonSettings = Seq(
  version := "0.1-SNAPSHOT",
  organization := "com.example",
  scalaVersion := "2.13.0",
  test in assembly := {}
)


lazy val app = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    mainClass in assembly := Some("Main"),
    assemblyJarName in assembly := "something.jar"
  )
