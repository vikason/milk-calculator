import org.joda.time.DateTime

import scala.collection.mutable
import scala.io.StdIn
import util.control.Breaks._

object MilkCalculator {

  def printMilkConfig(): Unit = {
    MilkConfig().milkPerDayConfig foreach println
  }

  def main(args: Array[String]) {
     if(args.length != 2) {
       throw new IllegalArgumentException("Please provide month and year to calculate balance")
     }
     val month:Int = args(0)
     val year:Int = args(1)

//    val month: Int = 1
//    val year: Int = 2019

    println(s"\n\n\nCalculating for Month: ${month} Year: ${year}");
    printMilkConfig
    val ltr: Int = new ProcessDateTime().getTotalMilkPerMonth(month, year)
    val missedLtr: Int = MissMilkCount.leaves(month, year)

    val previousDue = Utils.readNumberWithMessage("Previous Due amount (if any): ", 0)

    val config = MilkConfig()
    val totalDueAmount = printDetails(ltr, missedLtr, if(previousDue == -99999) 0 else previousDue, config)

    try processMoney(totalDueAmount)
    catch {case e: IllegalArgumentException => println(e.getMessage)}
  }

  def processMoney(totalDueAmount:BigDecimal): Unit = {
    // read the amount tendered
    if(Utils.askConfirmation("Do you want to pay now? (y/No):") != "y") return

    printf("Tender amount THB: ")
    val amountTendered = Utils.readBigDecimal

    if(amountTendered.compareTo(totalDueAmount) < 0)
      throw new IllegalArgumentException(s"ERROR: Sorry !!!! \nAmount tendered is less then due amount. You need THB ${totalDueAmount - amountTendered} more!!")

    // display balance amount
    val balanceAmount = amountTendered - totalDueAmount

    println(s"Your balance amount : THB ${balanceAmount}")

    // ask for confirmation
    if(Utils.askConfirmation("Save changes ? (y/No):")!= "y") return

    // todo: save data to txt file
    printf("NOT YET AVAILABLE! PLEASE KEEP THE DETAILS BY YOUR SELF")
  }

  implicit def toInt(value: String): Int = {
    Integer.parseInt(value)
  }

  private def printDetails(ltr: Int, missedLtr: Int,
                           previousDue: scala.BigDecimal, config: MilkConfig): BigDecimal = {
    println("Total Milk for this month in liters: " + ltr)
    println("Milk not taken this month in liters: " + missedLtr)
    println("----------------------------------------")
    println("Net Milk received this month 'X': " + (ltr - missedLtr))
    println("Price Per liters THB 'Y': " + config.pricePerLiter)
    println("Previous Due Amount THB 'P': " + previousDue)
    println("----------------------------------------")
    val netPayableAmount = (config.pricePerLiter.*(BigDecimal.apply(ltr - missedLtr))) - previousDue
    println("Net Payable amount THB '(X * Y)-P': THB " + netPayableAmount)
    println("----------------------------------------")
    println("----------------------------------------")

    netPayableAmount

  }
}

object Utils {

  def readNumberWithMessage(msg: String, defaultValue: Int) : Int = {
    printf(msg)
    readNumber(defaultValue)
  }

  def readNumber(defaultValue: Int) : Int = {
    val input = StdIn.readLine()

    if (input.equalsIgnoreCase("")) defaultValue
    else{
      try{
        Integer.parseInt(input)
      }catch{
        case e: NumberFormatException => -99999
      }
    }
  }

  def readBigDecimal: BigDecimal = {
    val input = StdIn.readLine()

    if (input.equalsIgnoreCase("")) 0
    else {
      try{
        BigDecimal.valueOf(java.lang.Long.valueOf(input))
      }catch{
        case e: NumberFormatException => 0
      }
    }
  }

  def askConfirmation(msg: String): String = {
    printf(msg)
    StdIn.readLine()
  }
}

object MissMilkCount {

  def leaves(month: Int, year: Int): Int = {
    var totalLtrMissed = 0
    breakable {
      while (true) {
        val input = Utils.readNumberWithMessage("\nEnter date when you not taken milk or Q for exit: ", 1)
        if(input == -99999) {
          return totalLtrMissed
        }

        val config = MilkConfig().milkPerDayConfig
        totalLtrMissed += config.getOrElse(new DateTime(year, month, input, 0, 0).dayOfWeek().getAsShortText.toUpperCase(), 0)
      }
    }
    totalLtrMissed
  }
}


class ProcessDateTime {
  def getTotalMilkPerMonth(month: Int, year: Int): Int = {
    val firstDateOfMonth = new DateTime(year, month, 1, 0, 0)
    val days = List(Weekdays.TUE.toString(), Weekdays.THU.toString(), Weekdays.SAT.toString())
    var count: Int = 1
    var ltr: Int = 0
    var evaluationDate = firstDateOfMonth
    val config = MilkConfig().milkPerDayConfig
    val maxDay = evaluationDate.dayOfMonth().withMaximumValue().getDayOfMonth()
    while (count <= maxDay) {
      if (days.contains(evaluationDate.dayOfWeek.getAsShortText.toUpperCase()))
        ltr += config.getOrElse(evaluationDate.dayOfWeek.getAsShortText.toUpperCase(), 0)
      evaluationDate = evaluationDate.plusDays(1)
      count += 1
    }
    ltr
  }

  private def getWeekIndex(value: Weekdays.Value): Int = {
    Weekdays.withName(value.toString).id + 1
  }
}

case class MilkConfig() {

  var weekStartDay: Weekdays.Value = Weekdays.MON
  var pricePerLiter: BigDecimal = BigDecimal.apply(37)
  var nameOfVendor: String = "Vedanta"
  var currency: String = "THB"
  var milkPerDayConfig = new mutable.LinkedHashMap[String, Int]()
  milkPerDayConfig.put("MON", 0)
  milkPerDayConfig.put("TUE", 2)
  milkPerDayConfig.put("WED", 0)
  milkPerDayConfig.put("THU", 2)
  milkPerDayConfig.put("FRI", 0)
  milkPerDayConfig.put("SAT", 3)
  milkPerDayConfig.put("SUN", 0)


}

object Weekdays extends Enumeration {
  type Weekdays = Value
  val MON, TUE, WED, THU, FRI, SAT, SUN = Value

}
